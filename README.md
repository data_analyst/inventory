# Store Inventory

## Decription

The Store Inventory is is a console application written in Python. The app uses an Sqlite database and makes use of Python's SQLite3 to xbinteract with that database.

Data is initially imported from a CSV file and and the database can be backed up at any time to a separate CSV file.

Users of the app can perform the following tasks:

* View all products in the inventory, one at a time.
* Add a new item to the inventory.
* Backup the inventory to an external CSV file. 


## Duplicate entries

The application will check for duplicate entires (based on the item's name) and will prompt the user to eithet keep the existing entry or overwrite with the newer information.

Duplicate enties are also checked during the initial CSV import.




<!--  LocalWords:  SQLIinteract Iinteract
 -->
