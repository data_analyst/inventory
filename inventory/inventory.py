""" Manage the inventory """
# import peewee
from datetime import datetime
from peewee import (
    SqliteDatabase,
    PrimaryKeyField,
    CharField,
    IntegerField,
    DecimalField,
    DateField,
    Model,
    Database,
)
from peewee import *
from playhouse import *
from playhouse.dataset import DataSet

# from playhouse.csv_loader import *  # for importing a csv

PATH = "/home/fede/Documents/Learn/Projects/inventory2/inventory/"


class Inventory:
    """ products and methods for managing the inventory """

    path = "/home/fede/Documents/Learn/Projects/inventory2/inventory/"

    def __init__(self):
        self.database = SqliteDatabase(self.path + "inventory.db")
        self.database.connect()  # do not forget to close
        # self.table = self.database["product"]

    def initialize_db(self):
        """ create the database inventory.db with an empty table Product """
        db = SqliteDatabase(self.path + "inventory.db")
        db.create_tables([Product])
        db.close()

    def test_db(self):
        """ ensure we can connect to the database """
        try:
            database = SqliteDatabase(self.path + "inventory.db")
            database.connect()
            print("success, we can connect to the sqlite db")
            database.close()
        except Exception as exception:
            print("ERROR DURING CONNECTION, do you want to import the csv?")
            # TODO: ask for input and run initialize_db if user wants
            print(exception)

    def remove_dollar(self):
        """ remove dollar symbol from a text file """
        with open(f"{PATH}inventory.csv", "r") as in_file, open(
            "/tmp/inventory.csv", "w"
        ) as out_file:
            temp = in_file.read().replace("$", "")
            out_file.write(temp)

    def import_file(self):
        """ import if properly formatted """
        # must remove dollar sign in order for it to work
        print("import file")
        self.remove_dollar()
        dataset = DataSet(f"sqlite:///{PATH}inventory.db")
        table = dataset["product"]
        table.thaw(filename=f"/tmp/inventory.csv", format="csv")

    def view_product(self, product_id):
        """ view data about a single product """
        # TODO: handle error when product_id not in db
        print("view_product")
        cursor = self.database.execute_sql(
            f"SELECT * from product where id = {product_id}"
        )
        for row in cursor.fetchone():
            print(row)

    def add_product(self, product_name, product_price, product_quantity):
        # OOP: should create a Product instance?
        self.database.execute_sql(
            f"INSERT into product(product_name, product_price, product_quantity, date_updated) values('{product_name}',{product_price},{product_quantity}, 1/1/1980)"
        )
        print(f'Product {product_name} added.')

    def backup(self):
        ''' creates a backup '''
        # TODO: create a backup folder
        # TODO: use csv instead of json
        # TODO: better name for backup files
        dataset = DataSet(f"sqlite:///{PATH}inventory.db")
        table = dataset["product"]
        dataset.freeze(table.all(), format='json', filename=f'{PATH}/backup-{datetime.now()}.json')

        
    def quit(self):
        """ close the database connection """
        self.database.close()
        print('Good bye')


# class BaseModel(Model):
#     """ BaseModel """
#
#     class Meta:
#         """ create the database """
#
#         db = SqliteDatabase(PATH + "inventory.db")
#         database = db
#         db_table = "product"


# the Product Model
class Product:
    # fields for peewee
    product_id = PrimaryKeyField()
    product_name = CharField()  # (unique=True)
    product_quantity = IntegerField()
    product_price = DecimalField()
    date_updated = DateField()

    def __init__(
        self, product_id, product_name, product_quantity, product_price, date_updated
    ):
        self.product_id = (product_id,)
        self.name = (product_name,)
        self.quantity = (product_quantity,)
        self.price = (product_price,)
        self.date_updated = date_updated

    # 
# def # ma# in():
#     ""# # " main """
#    pr# int("Welcome to Inventory Management")
#    # database = DataSet(f"sqlite:///{PATH}inventory.db")
#    # table = database["product"]
#    # database.close()


if __name__ == "__main__":
    main()
