""" main logic """
from inventory import Inventory

def main():
    """ main """
    inventory = Inventory()


if __name__ == "__main__":
    main()


MY_INVENTORY = Inventory()
MY_INVENTORY.initialize_db()
MY_INVENTORY.test_db()
