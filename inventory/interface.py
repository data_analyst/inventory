""" command line interface for managing the inventory """
import sys
from inventory import Inventory


class Menu:
    """ display a Text with choices """

    def __init__(self):
        self.inventory = Inventory()
        self.choices: dict = {
            "i": ("import data from csv", self.import_file),
            "v": ("view product", self.view_product),
            "a": ("add new product", self.add_product),
            "b": ("backup data", self.backup),
            "q": ("quit", self.exit)
        }

    def display_menu(self):
        """ shows the menu """
        for k in self.choices:
            print(f"{k}: {self.choices[k][0]}")

    def run(self):
        while True:
            self.display_menu()
            choice = input("Please enter an option ")
            action = self.choices.get(choice)[1]
            if action:
                print(action)
                action()
            else:
                print(
                    f"{choice} is not a valid option\n" +
                    "please enter a valid option"
                )

    def import_file(self):
        self.inventory.import_file()

    def view_product(self):
        print('view p')
        product_id = input("please enter the product id you'd like to view")
        self.inventory.view_product(product_id)


    def add_product(self):
        print('adding new product')
        product_name: str = input('Product name')
        product_price: float = input('Price')
        product_quantity: int = input('Quantity')
        self.inventory.add_product(product_name,
                                   product_price,
                                   product_quantity)

    def backup(self):
        print('backing up')
        self.inventory.backup()

    def exit(self):
        print('Good bye')
        sys.exit()


a = Menu()
a.run()








